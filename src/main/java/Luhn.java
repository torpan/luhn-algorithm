import java.util.Scanner;

public class Luhn {
    boolean LuhnAlgorithm(String Number) {

        int numberOfDigits = Number.length();

        int nSum = 0;
        boolean isSecond = false;
        for (int i = numberOfDigits - 1; i >= 0; i--) {

            int n = Number.charAt(i) - '0';

            if (isSecond) {
                n = n * 2;

                //If the added numbers are higher than 9 (double digits), remove 9 to get it to single digit again.
                if (n > 9) {
                     n -= 9;
                }
                nSum += n / 10;
                nSum += n % 10;

                isSecond = !isSecond;
            }

        }
        return (nSum % 10 == 0);
    }

    public String checkControlDigit(String notLastNumber, boolean valid) {

        char ch;
        int n;
        int count = 0;
        int sum = 0;
        String controlDigit;
        for (int i = notLastNumber.length() - 1; i >= 0; i--) {

            if (valid) {

                ch = notLastNumber.charAt(i);
                n = Character.getNumericValue(ch);
                count++;

                if (count % 2 == 0) {
                    n = n * 2;

                    //if the number gets higher than 9, I take that number minus 9, to get it to be a single number.
                    if (n > 9) {
                        n -= 9;
                    }
                    sum += n;
                }

            }
            else {
                return null;
            }
        }
        sum = (10 - (sum % 10));
        controlDigit = Integer.toString(sum);
        return controlDigit;
    }


    static public void main(String[] args) {
        Luhn l = new Luhn();

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number you want to check");
        String userInput = sc.next();
        int length = userInput.length();

        String notLastNumber = userInput.substring(0, userInput.length() - 1);



        if (l.LuhnAlgorithm(userInput) && length == 16) {
            //Creating two substrings to split the credit card number and get the first 15 in one and the last one in another.
            //This in order to get the correct output in the console.
            String lastNumber = userInput.substring(userInput.length() - 1, userInput.length());
            String firstNumbers = userInput.substring(0, 15);

            System.out.println("Input: "+firstNumbers+" "+lastNumber);
            System.out.println("Provided: "+lastNumber);
            System.out.println("Expected: "+l.checkControlDigit(notLastNumber, l.LuhnAlgorithm(userInput)));
            System.out.println("This is a valid number and seems to be a credit card");
        }
        else if (l.LuhnAlgorithm(userInput)){
            System.out.println("This is a valid number");
        }
        else {
            System.out.println("This is not a valid number");
            System.out.println("If you added a credit card number with spaces, try to remove the spaces.");
            }

    }

}

